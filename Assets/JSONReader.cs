﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Xml;
using System.Xml.Serialization;

public class JSONReader : MonoBehaviour
{
    public string wwwCall;
    public List<string> Titles;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GetText());


    }


    IEnumerator GetText()
    {
        UnityWebRequest www = UnityWebRequest.Get(wwwCall);
        yield return www.SendWebRequest();

        string xml = www.downloadHandler.text;
        Debug.Log(xml);

        //var myObjs = JsonUtility.FromJson<Wrapper>("{\"Search\":" + json + ",\"totalResults\":\"1450\",\"Response\":\"True\"}");

        Wrapper obj = new Wrapper();
        obj = JsonUtility.FromJson<Wrapper>(xml);

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(xml);

        XmlNodeList elemList = xmlDoc.GetElementsByTagName("title");

        Debug.Log(elemList.Count + " " + elemList[0]);

    }

    [System.Serializable]
    public class OMDB
    {
        public string Title;
        public string Year;
        public string imdbID;
        public string Type;
        public string Poster;
    }

    [System.Serializable]
    public class Wrapper
    {
        public OMDB[] omdb;
    }

    [System.Serializable]
    public class WrapperResult
    {
        public Wrapper[] wrappers;
    }
}
